<center><h1>Way-Pay【待支付平台】</h1></center>

> way-pay project Api Interface doc 【待支付平台接口定义文档】

?> *要点*：<br>1. 玩家充值成功调用返回付款二维码，默认`3分钟`页面刷新失效。<br>
2.通知回调接口，当玩家充值金额与交易金额不符时，需要手动做处理。<br>
3.`amount` 范围 `50 <= amount <= 30000` <br>
4.有问题，可随时更改页面接口以及文档 [📝 EDIT DOCUMENT](https://gitlab.com/zhangbiyu/apidoc/blob/master/docs/README.md)


<h1> Interface directory</h1>

!> 此处只定义了接口文档，具体调用地址后续沟通


* [API](/)
  * [Recharge【玩家充值接口】](/api/recharge)
  * [Notice【充值成功通知回调接口】](/api/recharge)
  * [Sign](/api/sign)
