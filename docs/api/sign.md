# Sign

?> 请求方法时需要在请求参数，或者返回结果上添加签名！下面会简要说明签名方法！

!> 在做签名时需要剔除参数，sign

#### 1. 请求参数

``` java
Map<String, String> params = new HashMap<>();
params.put("pid", "DWAYA1234WP191010203937Q4TZXC");
params.put("type", "2");
params.put("amount", "100");
params.put("sign", "9281edd14a693a98562fac5502e96ee0");
params.put("param", null);
```

#### 2. 剔除sign,如果参数为null也需要剔除,然后对请求参数做排序

``` java
// delete value is null and param is sign
Map<String, String> params = new HashMap<>();
params.put("amount", "100");
params.put("pid", "DWAYA1234WP191010203937Q4TZXC");
params.put("type", "2");
```

#### 3. 参数拼接

``` java
String param = "amount=100&pid=DWAYA1234WP191010203937Q4TZXC&type=2";
```

#### 4. MD5 加密

!> 加密的时候需要加上秘钥（secretkey）

``` java
String secretkey = "api_doc";
String sign = Md5Util.md5("amount=100&pid=DWAYA1234WP191010203937Q4TZXC&type=2"+secretkey);

```

#### 5. 请求接口的时候需要加上 sign
