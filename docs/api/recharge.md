# Recharge Interface

> 玩家充值接口


### URL 【地址】

?> [/api/recharge]()

!> 此接口以后续提到的正式地址为准，此处只统一传参和返回数据

### 请求方式    `POST`

### Params 【参数】

|  key   | type  | remarks  | example |
|  :----:  | :----:  | :----  |:----:  |
|  `pid`  | String  | transaction id【交易id】  |P123817231873|
|  `amount`  | String  | price【交易金额】  |500.00|
|  `type`  | String  | 交易类型：<br> 1. 微信 2. 支付宝  |1|
|  `sign`  | String  | [Sign](/api/sign)  |9281edd14a693a98562fac5502e96ee0|

### Response 【返回结果】

- 返回结果

|  key   | type  | remarks  | example |
|  :----:  | :----:  | :----  |:----:  |
|  `code`  | String  | 响应码：<br> `000` 成功，`999` 失败 |P123817231873|
|  `msg`  | String  | 响应信息 |成功|
|  `result`  | Object  | 响应数据  |{}|

- result 响应对象

|  key   | type  | remarks  | example |
|  :----:  | :----:  | :----  |:----:  |
|  `pid`  | String  | transaction id【交易id】  |P123817231873|
|  `amount`  | String  | price【交易金额】  |500.00|
|  `type`  | String  | 交易类型：<br> 1. 微信 2. 支付宝 3.银联 |1|
|  `orderNo`  | String  | 订单号 |OR201910020087|
|  `imageUrl`  | String  | 付款二维码 |url|
|  `sign`  | String  | [Sign](/api/sign)  |9281edd14a693a98562fac5502e96ee0|


### example


- success

``` json
{
  "code": "000",
  "msg": "success",
  "result": {
    "pid": "P192371626391273",
    "amount": 500,
    "type": "1",
    "orderNo": "OR201910020087",
    "imageUrl": "http://47.56.198.158/wfiles/images/2019/10/2d498dee4c2248d3834384809a1896a6.png",
    "sign":"9281edd14a693a98562fac5502e96ee0"
  }
}
```

- error

``` json
{
  "code": "210",
  "msg": "Top up system is busy now, please try again later"
}
```
