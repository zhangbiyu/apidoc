# Notice Interface

> way-pay 充值完成，通知回调接口


### URL 【地址】

?> [/api/notice]()

!> 此接口由你们提供具体地址为准，此处只统一传参和返回数据

### 请求方式    `POST`

### Params 【参数】

|  key   | type  | remarks  | example |
|  :----:  | :----:  | :----  |:----:  |
|  `pid`  | String  | transaction id【交易id】  |P123817231873|
|  `amount`  | String  | price【交易金额】  |500.00|
|  `price`  | String  | 玩家实际充值金额  |500.00|
|  `type`  | String  | 交易类型：<br> 1. 微信 2. 支付宝  |1|
|  `status`  | String  | 交易状态：<br> 1. 充值成功 <br>2. 充值失败 <br>3. 充值与交易金额不符  |1|
|  `sign`  | String  | [Sign](/api/sign)  |9281edd14a693a9|

### Response 【返回结果】

- 返回结果

|  key   | type  | remarks  | example |
|  :----:  | :----:  | :----  |:----:  |
|  `code`  | String  | 响应码：<br> `000` 成功，`999` 失败 |P123817231873|
|  `msg`  | String  | 响应信息 |成功|
|  `result`  | Object  | 响应数据  |{}|

### example

- success

``` json
{
  "code": "000",
  "msg": "成功",
  "result": {}
}
```

- error

``` json
{
  "code": "999",
  "msg": "Top up failed! Amount should 50 ~ 30000!"
}
```
